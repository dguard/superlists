from django.test import TestCase
from django.http import HttpRequest
from lists.models import Item, List
from django.utils.html import escape
from lists.forms import (
    DUPLICATE_ITEM_ERROR, EMPTY_LIST_ERROR,
    ExistingListItemForm, ItemForm,
)
from django.contrib.auth import get_user_model
from lists.views import new_list
from unittest.mock import Mock, patch
import unittest
User = get_user_model()

from lists.views import share_list, view_list, my_lists


class HomePageTest(TestCase):
    maxDiff = None

    def test_home_page_renders_home_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_home_page_uses_item_form(self):
        response = self.client.get('/')
        self.assertIsInstance(response.context['form'], ItemForm)


class NewListTest(TestCase):

    def test_for_invalid_input_renders_home_template(self):
        response = self.client.post('/lists/new', data={'text': ''})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    def test_for_invalid_input_passes_form(self):
        response = self.client.post('/lists/new', data={'text': ''})
        self.assertIsInstance(response.context['form'], ItemForm)

    def test_validating_errors_are_shown_on_home_page(self):
        response = self.client.post('/lists/new', data={'text': ''})
        self.assertContains(response, escape(EMPTY_LIST_ERROR))


class ListViewTest(TestCase):

    def post_invalid_input(self):
        list_ = List.objects.create()
        return self.client.post('/lists/%d/' % (list_.id,), data={'text': ''})

    def test_passes_correct_list_to_template(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()

        response = self.client.get('/lists/%d/' % (correct_list.id,))
        self.assertEqual(response.context['list'], correct_list)

    def test_uses_list_template(self):
        list_ = List.objects.create()
        response = self.client.get('/lists/%d/' % (list_.id,))
        self.assertTemplateUsed(response, 'list.html')

    def test_display_only_items_for_that_list(self):
        correct_list = List.objects.create()
        Item.objects.create(text="Item 1 in correct list", list=correct_list)
        Item.objects.create(text="Item 2 in correct list", list=correct_list)

        other_list = List.objects.create()
        Item.objects.create(text="Item 1 in other list", list=other_list)
        Item.objects.create(text="Item 2 in other list", list=other_list)

        response = self.client.get('/lists/%d/' % (correct_list.id,))
        self.assertContains(response, 'Item 1 in correct list')
        self.assertContains(response, 'Item 2 in correct list')
        self.assertNotContains(response, 'Item 1 in other list')
        self.assertNotContains(response, 'Item 2 in other list')

    def test_displays_all_items(self):
        list_ = List.objects.create()
        Item.objects.create(text='item 1', list=list_)
        Item.objects.create(text='item 2', list=list_)

        response = self.client.get('/lists/%d/' % (list_.id,))

        self.assertContains(response, 'item 1')
        self.assertContains(response, 'item 2')

    def test_saving_a_POST_request(self):
        self.client.post(
            '/lists/new',
            data={'text': 'A new list item'}
        )
        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new list item')

    def test_can_save_new_item_to_an_existing_list(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()

        self.client.post(
            '/lists/%d/' % (correct_list.id,),
            {'text': 'new item'}
        )
        self.assertEqual(Item.objects.count(), 1)

        first_item = Item.objects.first()
        self.assertEqual(first_item.text, 'new item')
        self.assertEqual(first_item.list, correct_list)

    def test_POST_redirects_to_list_view(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()

        response = self.client.post(
            '/lists/%d/' % (correct_list.id,),
            data={'text': 'A new list item'}
        )
        self.assertRedirects(response, '/lists/%d/' % (correct_list.id,))

    def test_displays_item_form(self):
        response = self.post_invalid_input()
        self.assertIsInstance(response.context['form'], ItemForm)
        self.assertContains(response, 'name="text"')

    def test_for_invalid_input_renders_list_template(self):
        response = self.post_invalid_input()
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'list.html')

    def test_for_invalid_input_passes_form_to_template(self):
        response = self.post_invalid_input()
        self.assertIsInstance(response.context['form'], ExistingListItemForm)

    def test_validating_errors_are_shown_on_home_page(self):
        response = self.post_invalid_input()
        self.assertContains(response, escape(EMPTY_LIST_ERROR))

    def test_invalid_input_nothing_saved_to_db(self):
        self.post_invalid_input()
        self.assertEqual(Item.objects.count(), 0)

    def test_displays_items_form(self):
        list_ = List.objects.create()
        response = self.client.get('/lists/%d/' % (list_.id,))
        self.assertIsInstance(response.context['form'], ExistingListItemForm)
        self.assertContains(response, 'name="text"')


class MyListsTest(TestCase):

    def test_my_lists_renders_template(self):
        User.objects.create(email='a@b.com')
        response = self.client.get('/lists/users/a@b.com/')
        self.assertTemplateUsed(response, 'my_lists.html')

    def test_passes_correct_owner_to_template(self):
        User.objects.create(email='wrong@owner.com')
        correct_owner = User.objects.create(email='correct@owner.com')
        response = self.client.get('/lists/users/correct@owner.com/')
        self.assertEqual(response.context['owner'], correct_owner)

    def test_list_owner_is_saved_if_user_is_authenticated(self):
        request = HttpRequest()
        request.user = User.objects.create(email='a@b.com')
        request.POST['text'] = 'new list item'
        new_list(request)
        list_ = List.objects.first()
        self.assertEqual(list_.owner, request.user)

    def test_my_list_renders_shared_with_lists(self):
        shared_user = User.objects.create(email='email@example.com')

        list_ = List.objects.create()
        Item.objects.create(text='first item', list=list_)
        list_.shared_with.add(shared_user)

        request = HttpRequest()
        request.user = shared_user

        response = my_lists(request, shared_user.email)
        self.assertContains(response, list_.name)

@patch('lists.views.NewListForm')
class NewListViewUnitTest(unittest.TestCase):

    def setUp(self):
        self.request = HttpRequest()
        self.request.POST['text'] = 'new list item'
        self.request.user = Mock()

    def test_passes_POST_data_to_NewListForm(self, mockNewListForm):
        new_list(self.request)
        mockNewListForm.assert_called_once_with(data=self.request.POST)

    def test_saves_form_with_owner_if_form_valid(self, mockNewListForm):
        mock_form = mockNewListForm.return_value
        mock_form.is_valid.return_valud = True
        new_list(self.request)
        mock_form.save.assert_called_once_with(owner=self.request.user)

    @patch('lists.views.redirect')
    def test_redirects_to_form_returned_object_if_form_valid(
        self, mock_redirect, mockNewListForm
    ):
        mock_form = mockNewListForm.return_value
        mock_form.is_valid.return_value = True

        response = new_list(self.request)

        self.assertEqual(response, mock_redirect.return_value)
        mock_redirect.assert_called_once_with(mock_form.save.return_value)

    @patch('lists.views.render')
    def test_renders_home_template_with_form_if_form_invalid(
        self, mock_render, mockNewListForm
    ):
        mock_form = mockNewListForm.return_value
        mock_form.is_valid.return_value = False

        response = new_list(self.request)
        self.assertEqual(response, mock_render.return_value)

        mock_render.assert_called_once_with(
            self.request, 'home.html', {'form': mock_form}
        )

    def test_does_not_save_if_form_invalid(self, mockNewListForm):
        mock_form = mockNewListForm.return_value
        mock_form.is_valid.return_value = False
        new_list(self.request)
        self.assertFalse(mock_form.save.called)

    @patch('lists.views.redirect')
    def test_redirects_to_form_returned_object_if_form_valid(
        self, mock_redirect, mockNewListForm
    ):
        mock_form = mockNewListForm.return_value
        mock_form.is_valid.return_value = True

        response = new_list(self.request)
        self.assertEqual(response, mock_redirect.return_value)

        mock_redirect.assert_called_once_with(mock_form.save.return_value)


class NewListViewIntegratedTest(TestCase):

    def test_saving_a_POST_request(self):
        self.client.post(
            '/lists/new',
            data={'text': 'A new list item'}
        )
        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new list item')

    def test_for_invalid_input_doesnt_save_but_shows_errors(self):
        response = self.client.post('/lists/new', data={'text': ''})
        self.assertEqual(List.objects.count(), 0)
        self.assertContains(response, escape(EMPTY_LIST_ERROR))

    def test_saves_list_owner_if_user_logged_in(self):
        request = HttpRequest()
        request.user = User.objects.create(email='a@b.com')
        request.POST['text'] = 'new list item'
        new_list(request)
        list_ = List.objects.first()
        self.assertEqual(list_.owner, request.user)


class ShareListTest(TestCase):

    def get_list_together_shared_with_and_owner(self):
        owner = User.objects.create(email='owner@examle.com')
        list_ = List.objects.create(owner=owner)
        shared_user = User.objects.create(email='shared@user.com')
        another_user = User.objects.create(email='another@user.com')

        list_.shared_with.add(shared_user.email)
        list_.shared_with.add(another_user.email)

        return list_

    def assert_response_matches(self, response, matches):
        self.assertRegexpMatches(response.content.decode(), '(\s|\S)*?'.join(matches))

    def assert_response_not_matches(self, response, matches):
        try:
            self.assertRegexpMatches(response.content.decode(), '(\s|\S)*?'.join(matches))
        except AssertionError:
            return
        self.fail('Regex "{regex}" matched string!'.format(
            regex='(\s|\S)*?'.join(matches)
        ))

    def test_post_redirects_to_list_view_page(self):
        user = User.objects.create(email='a@b.com')
        list_ = List.objects.create()
        response = self.client.post('/lists/%d/share/' % (list_.id,), data={'email': user.email})
        self.assertRedirects(response, '/lists/%d/' % (list_.id,))

    def test_adds_to_shared_with_user_if_post_is_valid(self):
        owner = User.objects.create(email='owner@examle.com')
        list_ = List.objects.create(owner=owner)

        shared_user = User.objects.create(email='shared@user.com')
        request = HttpRequest()
        request.POST['email'] = shared_user.email

        share_list(request, list_.id)
        self.assertIn(shared_user, list_.shared_with.all())

    def test_view_list_renders_shared_with_of_list_if_owner(self):
        list_ = self.get_list_together_shared_with_and_owner()
        request = HttpRequest()
        request.user = list_.owner

        response = view_list(request, list_.id)
        self.assert_response_matches(response, ['Shared with:', list_.shared_with.all()[0].email])
        self.assert_response_matches(response, ['Shared with:', list_.shared_with.all()[1].email])

    def test_view_list_does_not_render_shared_with_if_not_owner(self):
        list_ = self.get_list_together_shared_with_and_owner()
        request = HttpRequest()
        request.user = list_.shared_with.first()

        response = view_list(request, list_.id)
        self.assert_response_not_matches(response, ['Shared with:', list_.shared_with.first().email])

    def test_view_list_renders_no_shared_with_users_found_if_no_shared_with(self):
        list_ = List.objects.create()
        self.assertEqual(list_.shared_with.count(), 0)
        response = self.client.get('/lists/%d/' % (list_.id,))
        self.assertContains(response, 'This list not shared for users yet')

    def test_view_list_does_not_render_owner_if_user_is_owner(self):
        list_ = self.get_list_together_shared_with_and_owner()
        request = HttpRequest()
        request.user = list_.owner

        response = view_list(request, list_.id)
        self.assert_response_not_matches(response, ["List's owner:", list_.owner.email])

    def test_share_list_on_invalid_list_id_renders_404_page(self):
        self.assertEqual(List.objects.count(), 0)
        invalid_id = 1
        response = self.client.get('/lists/%d/share/' % (invalid_id,))
        self.assertEqual(response.status_code, 404)

    def test_share_list_on_post_without_correct_post_renders_404_page(self):
        list_ = List.objects.create()
        response = self.client.post('/lists/%d/share/' % (list_.id,))
        self.assertEqual(response.status_code, 404)

    # def test_share_list_return_content_on_redirect(self):
    #     list_ = List.objects.create()
    #
    #     with self.assertRaises(User.DoesNotExist):
    #         User.objects.get(email='not@exist.com')
    #
    #     response = self.client.post('/lists/%d/share/' % (list_.id,), data={'email': 'not@exist.com'})
    #     self.assertEqual(response.status_code, 302)
    #     self.assertEqual(list_.shared_with.count(), 0)
    #     self.assertContains(response.context['errors']['email'].text, 'User does not exist!')

    # def test_share_only_if_user_exist(self):
    #     list_ = List.objects.create()
    #
    #     with self.assertRaises(User.DoesNotExist):
    #         User.objects.get(email='not@exist.com')
    #
    #     response = self.client.post('/lists/%d/share/' % (list_.id,), data={'email': 'not@exist.com'})
    #     self.assertEqual(response.status_code, 302)
    #     self.assertEqual(list_.shared_with.count(), 0)
    #     self.assertContains(response.context['errors']['email'].text, 'User does not exist!')
    #
    # def test_does_not_share_already_shared_user(self):
    #     list_ = List.objects.create()
    #     shared_user = User.objects.create(email='shared@user.com')
    #     list_.shared_with.add(shared_user)
    #
    #     response = self.client.post('/lists/%d/share/' % (list_.id,), data={'email': shared_user.email})
    #     self.assertEqual(response.status_code, 302)
    #     self.assertEqual(list_.shared_with.count(), 1)
    #     self.assertContains(response.context['errors']['email'].text, 'You already share list to this user!')