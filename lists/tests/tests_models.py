from django.test import TestCase
from django.core.urlresolvers import resolve, reverse
from lists.views import home_page
from django.template.loader import render_to_string
from django.http import HttpRequest
from lists.models import Item, List
from django.core.exceptions import ValidationError
from django.utils.html import escape
from unittest import skip
from django.contrib.auth import get_user_model

User = get_user_model()


class ItemModelTest(TestCase):

    def test_default_text(self):
        item = Item()
        self.assertEqual(item.text, '')

    def test_item_is_related_to_list(self):
        list1 = List.objects.create()

        item = Item()
        item.text = 'bla bla'
        item.list = list1
        item.save()

        self.assertIn(item, list1.item_set.all())

    def test_cannot_save_empty_list_items(self):
        list_ = List.objects.create()
        item = Item(text="", list=list_)
        with self.assertRaises(ValidationError):
            item.save()
            item.full_clean()

    def test_duplicate_items_are_invalid(self):
        list_ = List.objects.create()
        Item.objects.create(text="bla bla", list=list_)
        with self.assertRaises(ValidationError):
            item = Item(text="bla bla", list=list_)
            item.full_clean()

    def test_can_save_duplicate_items_to_different_lists(self):
        list1 = List.objects.create()
        list2 = List.objects.create()
        Item.objects.create(text="bla bla", list=list1)
        item = Item(text="bla bla", list=list2)
        item.full_clean()  # should not raise

    def test_list_ordering(self):
        list1 = List.objects.create()
        item1 = Item.objects.create(text="i1", list=list1)
        item2 = Item.objects.create(text="item1", list=list1)
        item3 = Item.objects.create(text="3", list=list1)

        self.assertEqual(
            list(Item.objects.all()),
            [item1, item2, item3]
        )

    def test_string_representation(self):
        list1 = List.objects.create()
        item = Item.objects.create(text="some text", list=list1)
        self.assertEqual(str(item), "some text")

    # def test_for_invalid_input_shows_error_on_page(self):

    @skip
    def test_duplicate_item_validation_errors_end_up_on_lists_page(self):
        list1 = List.objects.create()
        item1 = Item.objects.create(text="some text", list=list1)

        response = self.client.post(
            '/lists/%d/' % (list1.id,),
            data={'text': 'some text'}
        )

        expected_error = escape("You've already got this in your list")
        self.assertContains(response, expected_error)
        self.assertTemplateUsed(response, 'list.html')
        self.assertEqual(Item.objects.count(), 1)


class ListModelTest(TestCase):

    def test_get_absolute_url(self):
        list_ = List.objects.create()
        self.assertEqual(list_.get_absolute_url(), '/lists/%d/' % (list_.id,))

    def test_create_new_creates_list_and_first_item(self):
        List.create_new(first_item_text='first item')
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'first item')
        new_list = List.objects.first()
        self.assertEqual(new_item.list, new_list)

    def test_create_new_optionally_saves_owner(self):
        user = User.objects.create()
        List.create_new(first_item_text='first item', owner=user)
        new_list = List.objects.first()
        self.assertEqual(new_list.owner, user)

    def test_list_can_have_owners(self):
        List(owner=User())  # should not raise

    def test_list_owner_is_optional(self):
        List().full_clean()  # should not raise

    def test_save_returns_new_list_object(self):
        returned = List.create_new(first_item_text='new item text')
        list_ = List.objects.first()
        self.assertEqual(returned, list_)

    def test_list_name_is_first_item_text(self):
        list_ = List.objects.create()
        first_item = Item.objects.create(list=list_, text='first item')
        Item.objects.create(list=list_, text='second item')
        self.assertEqual(list_.name, first_item.text)

    def test_shared_with_can_add_user_to_list(self):
        user = User.objects.create(email='a@b.com')

        list_ = List.objects.create()
        list_.shared_with.add(user.email)

        self.assertIn(user, list(list_.shared_with.all()))